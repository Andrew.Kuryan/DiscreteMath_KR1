package sample;

import java.util.Scanner;
import static java.lang.Math.*;

//класс для хранения решения системы лин. уравнений
class ResultSystem{
    double b[];
    int ks;
}

//интерфейс, описывающий общий вид функции для решения СЛАУ
interface CalcSystem{
    void calc_system(int n, double[][] a, ResultSystem ans);
}

public class Main{

    //минимальная константа, при делении на которую получается результат, отличный от нуля
    final static double eps = 1e-21;
    //переменная для считывания значений, введенных пользователем с клавиатуры
    static Scanner scan = new Scanner(System.in);

    public static int menu(){
        int a;
        System.out.println("\nВыберите действие: ");
        System.out.println("1 - решение СЛАУ методом Гаусса;");
        System.out.println("2 - решение СЛАУ методом квадратных корней Холецкого;");
        System.out.println("0 - выход");
        //получение значения из консоли
        a = scan.nextInt();
        return a;
    }

    public static void main(String[] args) {
        int c = -1;
        //пока пользователь не введет "0"
        while (c != 0){
            //запрос у пользователя желаемого действия
            c = menu();
            switch (c){
                case 1:
                    //передача в общий метод ссылки на требуемую функцию
                    calc_define_method(Main::SIMQ);
                    break;
                case 2:
                    //передача в общий метод ссылки на требуемую функцию
                    calc_define_method(Main::Holets);
                    break;
            }
        }
    }

    //реализация метода Гауса для решения СЛАУ
    public static void SIMQ(int n, double[][] a, ResultSystem ans){
        double max, u, v;
        int i, j, k1, l;
        //копирование столбца b в последний столбец матрицы a
        for (i=0; i<n; i++){
            a[i][n] = ans.b[i];
        }
        for (i=0; i<n; i++) {
            max = abs(a[i][i]);
            //позиция строки максимального элемента
            k1 = i;
            //поиск строки с максимальным элементом на позиции i
            for (l = i + 1; l < n; l++) {
                if (abs(a[l][i]) > max) {
                    max = abs(a[l][i]);
                    k1 = l;
                }
            }
            //предотвращение деления на число близкое к нулю
            if (max < eps) {
                ans.ks = 1;
                return;
            }
            //перемещение строки с максимальным элементом на позицию i
            if (k1 != i) {
                for (j = i; j < n + 1; j++) {
                    u = a[i][j];
                    a[i][j] = a[k1][j];
                    a[k1][j] = u;
                }
            }
            v = a[i][i];
            //деление строки i на первое значение в строке
            for (j=i; j< n+1; j++){
                a[i][j] = a[i][j] / v;
            }
            //вычитание из всех строк после i строки i, умноженной на первое значение строки l
            for (l=i+1; l<n; l++) {
                v = a[l][i];
                for (j = i + 1; j < n + 1; j++) {
                    a[l][j] = a[l][j] - a[i][j] * v;
                }
            }
        }
        //обратный ход метода, вычисление значений x
        for (i=n-1; i>=0; i--){
            ans.b[i] = a[i][n];
            for (j=i+1; j<n; j++){
                ans.b[i] = ans.b[i] - a[i][j] * ans.b[j];
            }
        }
    }

    //реализация метода квадратных корней Холецкого
    public static void Holets(int n, double[][] a, ResultSystem ans){
        int i, j, k;
        if (!check_on_symmetry(a)){
            ans.ks = 2;
            return;
        }
        //матрица, определяющая тип чисел в матрице a
        char[][] ca = new char[n][n];
        //матрица, определяющая тип чисел в стобце b
        char[] cb = new char[n];
        //заполнение матриц c по умолчанию: все числа являются действительными
        for (i=0; i<n; i++){
            for (j=0; j<n; j++){
                ca[i][j] = '0';
            }
        }
        for (i=0; i<n; i++){
            cb[i] = '0';
        }

        //составление матрицы L*L^T
        for (i=0; i<n; i++){
            //вычисление элементов вне главной диагонали
            for (j=0; j<i; j++){
                for (k=0; k<j; k++){
                    //учет возможности перемножения комплексных чисел
                    a[i][j] = a[i][j] - mult_i(ca[i][k], ca[j][k]) * a[i][k] * a[j][k];
                }
                //предотвращение деления на ноль
                if (a[j][j]<eps){
                    ans.ks = 1;
                    return;
                }
                a[i][j] = a[i][j] / a[j][j];
                //случай деления на комплексное число
                if (ca[j][j] == 'i'){
                    ca[i][j] = 'i';
                    a[i][j] = -a[i][j];
                }
            }
            //вычисление элементов на главной диагонали
            for (k=0; k<i; k++){
                //учет возможности перемножения комплексных чисел
                a[i][i] = a[i][i] - mult_i(ca[i][k], ca[i][k]) * a[i][k] * a[i][k];
            }
            //случай извлечения корня из отрицательного числа
            if (a[i][i] < 0){
                ca[i][i] = 'i';
                a[i][i] = abs(a[i][i]);
            }
            a[i][i] = sqrt(a[i][i]);
        }
        //вычисление столбца y
        for (i=0; i<n; i++){
            for (j=0; j<i; j++){
                //учет возможности перемножения комплексных чисел
                ans.b[i] = ans.b[i] - mult_i(ca[i][j], cb[j]) * a[i][j] * ans.b[j];
            }
            //предотвращение деления на число близкое к нулю
            if (a[i][i] < eps){
                ans.ks = 1;
                return;
            }
            ans.b[i] = ans.b[i] / a[i][i];
            //случай деления на комплексное число
            if (ca[i][i] == 'i'){
                cb[i] = 'i';
                ans.b[i] = -ans.b[i];
            }
        }
        //вычисление решения x
        for (i=n-1; i>=0; i--){
            for (j=n-1; j>i; j--){
                ans.b[i] = ans.b[i] - a[j][i] * ans.b[j];
            }
            ans.b[i] = ans.b[i] / a[i][i];
        }
    }

    //общий метод для работы с функциями, предназначенными для решения СЛАУ
    public static void calc_define_method(CalcSystem cs){
        int i, j, n;
        double a[][];
        //переменная для хранения решения системы лин. уравнений
        ResultSystem ans = new ResultSystem();

        System.out.println("Введите число уравнений системы: ");
        n = scan.nextInt();
        //инициализация матрицы коэффициентов
        //n+1 для метода Гаусса
        a = new double[n][n+1];
        //инициализация матрицы-столбца свободных членов
        ans.b = new double[n];
        //ввод матрицы коэффициентов
        for (i=0; i<n; i++){
            for (j=0; j<n; j++){
                System.out.print("a["+(i+1)+"]["+(j+1)+"] = ");
                a[i][j] = scan.nextDouble();
            }
        }
        //ввод столбца свободных членов
        for (i=0; i<n; i++){
            System.out.print("b["+(i+1)+"] = ");
            ans.b[i] = scan.nextDouble();
        }
        System.out.println("Введенная матрица:");
        output_matr(a);

        //вызов функции, переданной по ссылке с полученными параметрами
        cs.calc_system(n, a, ans);
        if (ans.ks==0){
            System.out.println("Решение: ");
            output_matr(ans.b);
        }
        else if (ans.ks == 2){
            System.out.println("Матрица не симметрична");
        }
        else{
            System.out.println("Система не имеет решений");
        }
    }

    //функция для перемножения чисел i
    public static int mult_i(char c1, char c2){
        //если оба числа являются вещественными
        if (c1 == '0' && c2 == '0'){
            return 1;
        }
        //если оба числа являются комплексными
        else if (c1 == 'i' && c2 == 'i'){
            return -1;
        }
        else{
            return 0;
        }
    }

    //проверка на симметричность матрицы
    public static boolean check_on_symmetry(double[][] a){
        int i, j;
        for (i = 1; i<a.length; i++){
            for (j=0; j<i; j++){
                if (a[i][j] != a[j][i]){
                    return false;
                }
            }
        }
        return true;
    }

    //вывод матрицы
    public static void output_matr(double[][] a){
        int i, j;
        for (i=0; i<a.length; i++){
            for (j=0; j<a[i].length; j++){
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }

    //вывод матрицы-столбца (переопределенный метод)
    public static void output_matr(double a[]){
        int i;
        for (i=0; i<a.length; i++){
            System.out.println("x["+(i+1)+"] = "+a[i]);
        }
    }
}